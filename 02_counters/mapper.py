#!/usr/bin/env python3

import sys
import re


stop_words = set()
with open("stop_words_en.txt") as fd:
    for line in fd:
        stop_words.add(line.strip())

for line in sys.stdin:
    try:
        article_id, text = line.strip().split('\t', 1)
    except ValueError as e:
        continue
    words = re.split("\W*\s+\W*", text)
    word_stat = dict()
    for word in words:
        if word.lower() in word_stat:
            word_stat[word.lower()] += 1
        else:
            word_stat[word.lower()] = 1
    for word, count in word_stat.items():
        print("reporter:counter:Wiki stats,Total words,%d" % count, file=sys.stderr)
        if word in stop_words:
            print("reporter:counter:Wiki stats,Stop words,%d" % count, file=sys.stderr)
        print("%s\t%d" % (word, count))
