OUT_DIR="02_cnr_out"
NUM_REDUCERS=4

hdfs dfs -rm -r -skipTrash ${OUT_DIR}

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="Assignment MR task1 step1" \
    -D mapreduce.job.reduces=$NUM_REDUCERS \
    -files mapper.py,reducer.py,/datasets/stop_words_en.txt \
    -mapper ./mapper.py \
    -reducer ./reducer.py \
    -input /data/wiki/en_articles_part \
    -output ${OUT_DIR}
