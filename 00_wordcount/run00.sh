#!/usr/bin/env bash

# Mapper only

OUT_DIR="streaming_wc_result"

hadoop fs -rm -r -skipTrash $OUT_DIR*

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="my_wordcout_example" \
    -files mapper.py \
    -mapper mapper.py \
    -input /data/wiki/en_articles_part \
    -output $OUT_DIR
