#!/usr/bin/env python3

import sys
import re

for line in sys.stdin:
    try:
        article_id, text = line.strip().split('\t', 1)
    except ValueError as e:
        continue
    words = re.split('\W*\s+\W*', text, re.UNICODE)
    for word in words:
        print("%s\t%d" % (word.lower(), 1))
