#!/usr/bin/env python3

import sys


for line in sys.stdin:
    try:
        key, value = line.strip().split('\t', 1)
        key = -int(key)
    except ValueError as e:
        continue
    print("%s\t%s" % (value, key))

